# **Praktikum Modul 3**

## **Anggota Kelompok**

| NRP        | Nama                      |
| ---        | ---                       |
| 5025211235 | Ketut Arda Putra Mahotama |
| 5025211003 | Clarissa Luna Maheswari   |
| 5025211127 | Nadif Mustafa             |

<br>

## **Daftar Isi**

- [**Praktikum Modul 3**](#praktikum-modul-3)
  - [**Anggota Kelompok**](#anggota-kelompok)
  - [**Daftar Isi**](#daftar-isi)
  - [**Soal 1**](#soal-1)
    - [**Screenshot Hasil**](#screenshot-hasil)
    - [**Cara Pengerjaan**](#cara-pengerjaan)
      - [**Penjelasan Functions**](#penjelasan-functions)
      - [**1A**](#1a)
      - [**1B**](#1b)
      - [**1C**](#1c)
      - [**1D**](#1d)
      - [**1E**](#1e)
    - [**Kendala yang Dialami**](#kendala-yang-dialami)
    - [**Revisi**](#revisi)
  - [**Soal 2**](#soal-2)
    - [**Screenshot Hasil**](#screenshot-hasil-1)
    - [**Cara Pengerjaan**](#cara-pengerjaan-1)
    - [**Kendala yang Dialami**](#kendala-yang-dialami-1)
    - [**Revisi**](#revisi-1)
  - [**Soal 3**](#soal-3)
    - [**Screenshot Hasil**](#screenshot-hasil-2)
    - [**Cara Pengerjaan**](#cara-pengerjaan-2)
    - [**Kendala yang Dialami**](#kendala-yang-dialami-2)
    - [**Revisi**](#revisi-2)
  - [**Soal 4**](#soal-4)
    - [**Screenshot Hasil**](#screenshot-hasil-3)
    - [**Cara Pengerjaan**](#cara-pengerjaan-3)
      - [**unzip.c**](#unzipc)
      - [**categorize.c**](#categorizec)
      - [**logchecker.c**](#logcheckerc)
    - [**Kendala yang Dialami**](#kendala-yang-dialami-3)
    - [**Revisi**](#revisi-3)

<br>

## **Soal 1**

### **Screenshot Hasil**

![terminal_soal1](/uploads/fea8ab2ba93cba37884d1faeafdbfbb1/image.png)

### **Cara Pengerjaan**

#### **Penjelasan Functions**

Pada soal ini, kami membuat `Huffman Tree` yang menerapkan `Minimum Heap`, yaitu suatu `Heap` yang nilai dari `top`-nya adalah minimum. Berikut adalah struct untuk `Node` dari `Min Heap :`

```c
typedef struct MinHeapNode
{
    char key;
    int val;
    struct MinHeapNode *left, *right;
}
HNode;
```

- `key :` menyimpan huruf (alfabet) yang muncul pada teks. Nantinya `key` juga akan bernilai karakter `'*'` untuk `Node` yang merupakan gabungan / parent dari `Node` lain.
- `val :` menyimpan frekuensi kemunculan suatu huruf.
- `left :` menyimpan pointer ke `Child Node` kiri.
- `right :` menyimpan pointer ke `Child Node` kanan.

<br>

Berikut adalah struct untuk `Min Heap :`

```c
typedef struct MinHeap
{
    unsigned size, capacity;
    HNode **arr;
}
Heap;
```

- `size :` menyimpan ukuran asli dari `Min Heap`
- `capacity :` menyimpan ukuran awal dari `Min Heap` setelah di-`create`.
- `arr :` menyimpan `Node` dalam bentuk array

<br>

Berikut adalah *utility function* untuk membuat suatu `Min Heap Node` baru :

```c
HNode *createNode(char key, int val)
{
    HNode *newNode = (HNode *) malloc(sizeof(HNode));
    newNode->key = key;
    newNode->val = val;
    newNode->left = newNode->right = NULL;
    return newNode;
}
```

- Menerima argument berupa `key` dan `val`.
- Alokasi memori untuk `Node` baru.
- Inisialisasi isi-isi dari `Node`.
- Mengembalikan pointer dari `Node`.

<br>

Berikut adalah *utility function* untuk membuat `Heap` baru :

```c
Heap *createHeap(int capacity)
{
    Heap *newHeap = (Heap *) malloc(sizeof(Heap));
    newHeap->size = 0;
    newHeap->capacity = capacity;
    newHeap->arr = (HNode **) malloc(capacity * sizeof(HNode *));
    return newHeap;
}
```

- Menerima argument berupa `capacity`.
- Alokasi memori untuk `Heap` baru.
- Inisialisasi isi-isi dari `Heap`.
- Mengembalikan pointer dari `Heap`.

<br>

Berikut adalah function untuk menukar isi dari 2 `Node :`

```c
void swapNode(HNode **a, HNode **b)
{
    HNode *temp = *a;
    *a = *b;
    *b = temp;
}
```

- Menerima argument berupa `pointer to pointer to Node` dari kedua `Node` yang akan ditukar.
- Membuat variabel temporary.
- Lakukan penukaran dengan memanfaatkan variabel temporary.

<br>

Berikut adalah *utility function* untuk menjaga supaya `Heap` tetap valid :

```c
void heapify(Heap *heap, int idx)
{
    int min = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if(left < heap->size && heap->arr[left]->val < heap->arr[min]->val)
        min = left;
    if(right < heap->size && heap->arr[right]->val < heap->arr[min]->val)
        min = right;
    if(min != idx)
    {
        swapNode(&heap->arr[idx], &heap->arr[min]);
        heapify(heap, min);
    }
}
```

- Mencari nilai minimum antara `Node` saat ini, `left child`, dan `right child`.
- Jika yang memiliki nilai minimum adalah `left` atau `right`, maka tukar `Node` tersebut dengan `parent`, kemudian panggil `heapify()` secara rekursif mulai dari `Node` tersebut.

<br>

Berikut adalah function untuk mendapatkan `Node` yang berada di `top` dan melakukan `pop :`

```c
HNode *getMin(Heap *heap)
{
    HNode *temp = heap->arr[0];
    heap->arr[0] = heap->arr[heap->size - 1];

    heap->size--;
    heapify(heap, 0);
    return temp;
}
```

- Simpan `top Node` pada variabel temporary.
- Pindahkan `Node` terakhir ke `top`.
- *Decrement* nilai `size`.
- Panggil `heapify()` muali dari `top`.
- Kembalikan pointer dari `top Node` lama menggunakan variabel temporary.

<br>

Berikut adalah function untuk insert `Node` baru :

```c
void insertNode(Heap *heap, HNode *node)
{
    heap->size++;
    int cur = heap->size - 1;
    int par = (cur - 1) / 2;

    while(cur > 0 && node->val < heap->arr[par]->val)
    {
        heap->arr[cur] = heap->arr[par];
        cur = par;
        par = (cur - 1) / 2;
    }
    heap->arr[cur] = node;
}
```

- Tambah `size` dari `Heap`.
- Letakkan `Node` baru pada posisi terakhir.
- Pindahkan terus `Node` baru ke parent dan ancestor nya hingga mencapai `top` atau tidak ada lagi ancestor yang `val`-nya lebih besar dari `Node` tersebut.

<br>

Berikut adalah *main function* untuk menjaga `Heap` supaya tetap valid :

```c
void buildHeap(Heap *heap)
{
    for (int i = (heap->size - 1) / 2; i >= 0; --i)
        heapify(heap, i);
}
```

<br>

Berikut adalah *main function* untuk membuat `Heap` baru :

```c
Heap *initHeap(char key[], int val[], int size)
{
    Heap *heap = createHeap(size);
    for (int i = 0; i < size; ++i)
        heap->arr[i] = createNode(key[i], val[i]);

    heap->size = size;
    buildHeap(heap);
    return heap;
}
```

<br>

Berikut adalah function untuk membuat `Huffman Tree :`

```c
HNode *buildTree(char key[], int val[], int size)
{
    HNode *left, *right, *par;
    Heap *heap = initHeap(key, val, size);

    while(heap->size > 1)
    {
        left = getMin(heap);
        right = getMin(heap);
        par = createNode('*', left->val + right->val);

        par->left = left;
        par->right = right;
        insertNode(heap, par);
    }
    return getMin(heap);
}
```

- Buat `Heap` baru yang berisi huruf-huruf yang muncul pada teks.
- Ambil 2 `Node` dengan `val` minimum, kemudian buat `Node` baru sebagai parent dengan nilai merupakan gabungan dari 2 `Node` tersebut.
- Ulangi langkah kedua hingga `size` dari `Heap` tersisa 1, yang berarti sudah terdapat `root` dari `Tree` yang merupakan gabungan dari semua `Node` awal.

<br>

Berikut adalah function untuk menyimpan `Huffman Code` dari masing-masing huruf :

```c
void encode(int bit[], int digit, char code[][50])
{
    for (int i = 0; i < digit; ++i)
        code[key_idx][i] = '0' + bit[i];
}
```

<br>

Berikut adalah function untuk mendapatkan `Huffman Code` (*encoding*) dari masing-masing huruf :

```c
void getCode(HNode *node, int bit[], int depth, char code[][50], char codeKey[])
{
    if(node->left){
        bit[depth] = 0;
        getCode(node->left, bit, depth + 1, code, codeKey);
    }
    if(node->right){
        bit[depth] = 1;
        getCode(node->right, bit, depth + 1, code, codeKey);
    }
    if (!node->left && !node->right)
    {
        codeKey[key_idx] = node->key;
        encode(bit, depth, code);
        key_idx++;
    }
}
```

- `node :` posisi `Node` sekarang.
- `bit[] :` menyimpan `Code` sekarang.
- `depth :` kedalaman `Node` / panjang `Code` sekarang.
- `code[][] :` menyimpan `Code` dari masing-masing huruf.
- `codeKey[] :` menyimpan huruf dari suatu index `Code`.
- Function ini melakukan *traversing* pada `Huffman Tree` secara `inorder`.
- Tambahkan `'0'` pada belakang `Code` ketika bergerak ke kiri, serta tambahkan `'1'` pada belakang `Code` ketika bergerak ke kanan.
- Jika menemukan `leaf`, artinya kita menemukan suatu huruf, maka simpan `Code` saat ini sebagai `Huffman Code` dari huruf tersebut.

<br>

Berikut adalah *main function* pada `Huffman Tree :`

```c
HNode *Huffman(char key[], int val[], int size, char code[][50], char codeKey[])
{
    HNode *node = buildTree(key, val, size);
    int bit[50], depth = 0;
    getCode(node, bit, depth, code, codeKey);
    return node;
}
```

- Buat `Huffman Tree`.
- Lakukan *encoding*.
- Kembalikan pointer dari `root` pada `Huffman Tree`. 

#### **1A**

> Pada *parent process*, baca *file* yang akan dikompresi dan hitung frekuensi kemunculan huruf pada *file* tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke *child process*.

Berikut adalah potongan kode untuk soal **1A** :

```c
else if(child_id > 0)   // parent
{
    /* 1A */
    char filename[] = "file.txt";
    char str[1000];

    int count[30], ct_alpha = 0;
    memset(count, 0, sizeof(count));

    FILE *fp;
    fp = fopen(filename, "r");

    if(fp == NULL){
        printf("Failed to open %s\n", filename);
        exit(1);
    }
    // Perhitungan Frekuensi
    while(fgets(str, 1000, fp) != NULL)
    {
        for (int i = 0; str[i]; ++i){
            if(isalpha(str[i])){
                count[toupper(str[i]) - 'A']++;
                ct_alpha++;
            }
        }
    }
    fclose(fp);

    // Tulis hasil perhitungan frekuensi ke pipe 1
    close(fd1[0]);
    write(fd1[1], count, sizeof(count));
    close(fd1[1]);
    wait(NULL)

    // Lanjutan pada parent process (1D dan 1E)
}
```

- Baca file `"file.txt"` dan hitung frekuensi untuk setiap huruf (alfabet) yang ditemukan (disimpan sebagai huruf kapital).
- Kirim hasil perhitungan frekuensi yang sudah disimpan pada array `count` ke `Pipe 1`.
- Tunggu hingga `child process` mengirimkan respon.

#### **1B**

> Pada *child process*, lakukan kompresi *file* dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

Berikut adalah potongan kode untuk soal **1B** :

```c
else if(child_id == 0)  // child
{
    /* 1B */
    int count[30], size = 0;
    char key[30];

    // Membaca hasil perhitungan frekuensi dari pipe 1
    close(fd1[1]);
    read(fd1[0], count, sizeof(count));
    close(fd1[0]);

    // Membuat Leaf dari Huffman Tree
    for(int i = 0; i < 26; ++i){
        if(count[i] > 0){
            key[size] = 'A' + i;
            count[size++] = count[i];
        }
    }
    char code[30][50], codeKey[30];
    HNode *root = Huffman(key, count, size, code, codeKey);

    // Lanjutan pada child process (1C)
}
```

- Membaca `Pipe 1` dan menyimpan hasil perhitungan frekuensi pada array `count`.
- Membuat list huruf yang muncul pada teks dan disimpan pada array `key`.
- Membuat `Huffman Tree` berdasarkan list huruf tersebut beserta frekuensi kemunculannya.

#### **1C**

> Pada *child process*, simpan Huffman *tree* pada file terkompresi. Untuk setiap huruf pada *file*, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

Berikut adalah potongan kode untuk soal **1C** :

```c
else if(child_id == 0)  // child
{
    // Kode awalan dari child process (1B)

    /* 1C */
    // Lakukan Kompresi
    char filename[] = "file.txt";
    char str[1000];
    char compressed[4000] = "";
    FILE *fp;
    fp = fopen(filename, "r");

    if(fp == NULL){
        printf("Failed to open %s\n", filename);
        exit(1);
    }
    while(fgets(str, 1000, fp) != NULL)
    {
        for(int i = 0; str[i]; ++i)
        {
            if(isalpha(str[i])){
                str[i] = toupper(str[i]);

                for(int j = 0; j < strlen(codeKey); ++j){
                    if(str[i] == codeKey[j])
                        strcat(compressed, code[j]);
                }
            }
        }
    }
    fclose(fp);

    // Kirim hasil kompresi ke pipe 2
    close(fd2[0]);
    write(fd2[1], codeKey, sizeof(codeKey));
    write(fd2[1], code, sizeof(code));
    write(fd2[1], huffman, sizeof(huffman));
    write(fd2[1], compressed, sizeof(compressed));
    close(fd2[1]);

    exit(0);
}
```

- Baca file `"file.txt"`, lakukan *encoding* pada setiap huruf (alfabet) yang ditemukan, kemudian simpan hasilnya pada string `compressed`.
- Kirim list huruf yang muncul (`codeKey`), code setiap huruf (`code`), root dari Huffman Tree (`huffman`), serta hasil kompresi teks (`compressed`) ke `Pipe 2`.

#### **1D**

> Kirim hasil kompresi ke *parent process*. Lalu, di *parent process* baca Huffman *tree* dari *file* terkompresi. Baca kode Huffman dan lakukan dekompresi.

Berikut adalah potongan kode untuk soal **1D** :

```c
else if(child_id > 0) // parent
{
    // Kode awalan dari parent process (1A)

    /* 1D */
    wait(NULL);
    char codeKey[30];
    char code[30][50];
    char compressed[4000];
    HNode *root, *node;

    // Membaca hasil kompresi dari pipe 2
    close(fd2[1]);
    read(fd2[0], codeKey, sizeof(codeKey));
    read(fd2[0], code, sizeof(code));
    read(fd2[0], root, sizeof(root));
    read(fd2[0], compressed, sizeof(compressed));
    close(fd2[0]);

    int len = strlen(compressed);
    node = root;
    printf("Hasil Dekompresi : ");

    for(int i = 0; i < len; ++i)
    {
        if(!node->left && !node->right){
            printf("%c", node->key);
            node = root;
        }
        if(compressed[i] == '0')
            node = node->left;
        else
            node = node->right;
    }
    printf("\n\n");

    // Lanjutan pada parent process (1E)
}
```

- Membaca `Pipe 2` yang berisi `code` masing-masing huruf, teks yang sudah dikompresi, serta pointer `Node` dari `Huffman Tree`.
- Melakukan *decoding* pada string `compressed`, yaitu dengan melakukan *traversing* pada `Huffman Tree`. Bergerak ke `left` ketika menemukan karakter `'0'`, bergerak ke `right` ketika menemukan karakter `'1'`, lakukan print kemudian kembali ke `root` ketika menemukan karakter huruf (alfabet).

#### **1E**

> Di *parent process*, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

Berikut adalah potongan kode untuk soal **1E** :

```c
else if(child_id > 0) // parent
{
    // Kode awalan dari parent process (1A dan 1D)

    printf("Total bit sebelum dikompresi : %d\n", ct_alpha);
    pritnf("Total bit setelah dikompresi : %ed\n", len);

    exit(0);
}
```

- Tampilkan banyaknya bit yang dibutuhkan untuk menyimpan teks sebelum dan setelah dikompresi.
- Sebelum dikompresi, teks disimpan dengan memanfaatkan ASCII. Pada ASCII, ukuran untuk setiap karakter adalah 8 bit. Sehingga total bit sebelum dikompresi adalah banyaknya huruf dikali 8.
- Setelah dikompresi, teks disimpan dengan memanfaatkan `Huffman Tree`. Pada `Huffman Tree`, ukuran untuk setiap karakter tergantung dari frekuensi kemunculannya, semakin besar frekuensinya, maka semakin kecil ukurannya. Karena pada *child process* kita sudah melakukan kompresi, maka cukup print panjang dari string yang sudah dikompresi.

### **Kendala yang Dialami**

Terjadi `Segmentation Fault` ketika mengirimkan `HNode *huffman` dari *child process* ke *parent process*. Hal tersebut dikarenakan di dalam `HNode *huffman` juga terdapat pointer `HNode` yaitu `left` dan `right`, sedangkan 2 process yang berbeda menggunakan memori yang berbeda juga. Oleh karena itu, terjadi error ketika *parent process* mencoba membaca pointer yang dikirim dari *child process*.

### **Revisi**

Salah satu cara untuk mengirim *pointer struct* dalam *pointer struct* melalui `Pipe` adalah dengan cara mengirim langsung *content-content* asli dari struct tersebut. Pada soal ini, tujuan *child process* mengirim `HNode *huffman` adalah untuk proses *decoding* pada *parent process*, karena proses *decoding* membutuhkan proses *traversing* pada `Huffman Tree`. Oleh karena itu, solusinya adalah `Huffman Tree` direpresentasikan menggunakan array, kemudian pointer dari array tersebut dikirim melalui `Pipe`.

Dibutuhkan struct baru untuk menyimpan `key` dan `val` dari masing-masing `Node` yang disimpan pada array. Berikut adalah kode untuk struct tersebut :

```c
typedef struct HeapData
{
    char key;
    int val;
}
HData;
```

<br>

Dibutuhkan function baru untuk memindahkan isi dari `Huffman Tree` ke dalam array. Berikut adalah kode untuk function tersebut :

```c
void preorder(HNode *node, HData newHeap[], int idx)
{
    newHeap[idx].key = node->key;
    newHeap[idx].val = node->val;

    if(isalpha(node->key))
        return;
    if(node->left)
        preorder(node->left, newHeap, 2 * idx + 1);
    if(node->right)
        preorder(node->right, newHeap, 2 * idx + 2);
}
```

- Function di atas melakukan *traversing* pada `Huffman Tree` secara *preorder*. *Indexing* pada array menggunakan konsep *indexing* yang sama dengan `Heap`, yaitu `root` berada di index 0, kemudian `left` adalah `2 * index + 1` dan `right` adalah `2 * index + 2`.

<br>

Terdapat tambahan proses di antara setelah menjalankan algoritma `Huffman` dan sebelum melakukan kompresi. Proses ini berfungsi untuk memindahkan isi dari `Huffman Tree` ke array dan mengubah `size` (banyaknya huruf berbeda yang muncul) menjadi string, supaya bisa dikirim ke `Pipe`. Berikut adalah potongan kode untuk proses tersebut :

```c
else if(child_id == 0)
{
    // Kode awalan dari child process (1B)

    int max_depth = 0;
    for(int i = 0; i < size; ++i){
        if(strlen(code[i]) > max_depth)
            max_depth = strlen(code[i]);
    }
    int newSize = (1 << (max_depth + 1)) - 1;
    char newSize_str[10] = "";
    sprintf(newSize_str, "%d", newSize);

    HData newHeap[newSize];
    for(int i = 0; i < newSize; ++i){
        newHeap[i].key = ' ';
        newHeap[i].val = 0;
    }
    preorder(root, newHeap, 0);

    // Lanjutan kode dari child process (1C)
}
```

- Proses pertama adalah untuk mencari kedalaman terdalam dari suatu huruf (alfabet), dengan kata lain panjang bit terpanjang dari `code` suatu huruf.
- Proses kedua adalah men-*convert* `size` menjadi string dengan memanfaatkan `sprintf()`.
- Proses ketiga adalah memindahkan isi dari `Huffman Tree` ke array `HData newHeap[]` dengan memanfaatkan function `preorder()`, setiap index yang tidak merepresentasikan `Node` dari `Huffman Tree` diisi dengan `key = ' '` dan `val = 0`.

<br>

Terdapat 2 perubahan dalam proses `write` ke `Pipe 2`, yaitu melakukan `write` untuk `newSize_str` dan `newHeap`. Berikut adalah potongan kode untuk proses `write` pada `Pipe 2 :`

```c
else if(child_id == 0)
{
    // Kode awalan dari child process (1B)

    // Kirim hasil kompresi ke pipe 2
    close(fd2[0]);
    write(fd2[1], codeKey, sizeof(codeKey));
    write(fd2[1], code, sizeof(code));
    write(fd2[1], compressed, sizeof(compressed));

    write(fd2[1], newSize_str, sizeof(newSize_str));
    write(fd2[1], newHeap, sizeof(newHeap));
    close(fd2[1]);

    exit(0);
}
```

<br>

Terdapat perubahan pada *parent process* pada proses membaca `Pipe 2` serta proses dekompresi. Hal tersebut dikarenakan yang dikirim saat ini adalah array, bukan lagi pointer `HNode`. Berikut adalah potongan kode untuk proses tersebut :

```c
else if(child_id > 0)
{
    // Kode awalan dari parent process (1A)

    /* 1D */
    wait(NULL);
    char codeKey[30];
    char code[30][50];
    char compressed[4000];
    char newSize_str[10];
    int newSize = 0;

    // Membaca hasil kompresi dari pipe 2
    close(fd2[1]);
    read(fd2[0], codeKey, sizeof(codeKey));
    read(fd2[0], code, sizeof(code));
    read(fd2[0], compressed, sizeof(compressed));
    read(fd2[0], newSize_str, sizeof(newSize_str));

    newSize = atoi(newSize_str);
    HData newHeap[newSize];

    read(fd2[0], newHeap, sizeof(newHeap));
    close(fd2[0]);

    int len = strlen(compressed);
    int idx = 0;
    printf("Hasil Dekompresi :\n");

    for (int i = 0; i < len; ++i)
    {
        if(isalpha(newHeap[idx].key)){
            printf("%c", newHeap[idx].key);
            idx = 0;
        }
        if(compressed[i] == '0')
            idx = 2 * idx + 1;
        else
            idx = 2 * idx + 2;
    }
    printf("%c\n\n", newHeap[idx].key);

    // Lanjutan kode pada parent process (1E)
}
```

- Membaca `Pipe 2` dan menyimpannya.
- Proses dekompresi mirip dengan sebelum revisi, perbedaannya hanyalah pada *traversing*. Karena traversing pada array menggunakan konsep `Heap`, maka ketika menemukan karakter `'0'` maka `index = 2 * index + 1`, serta ketika menemukan karakter `'1'` maka `index = 2 * index + 2`.

<br>

## **Soal 2**

### **Screenshot Hasil**
Soal 2a:

![2a](/uploads/e38b5f9fe7fd5093f30275c455680e58/2a.PNG)

Soal 2b: 

![2b](/uploads/c2269b9d04ab9432b38786a24f982b98/2b.PNG)


Soal 2c: 

![2c](/uploads/de4d19c25b36669dd65bbc5701ca878f/2c.PNG)

### **Cara Pengerjaan**
No 2a:

    Menggunakan Shared Memory:
        Program ini menggunakan shared memory untuk berbagi data antara proses yang berjalan secara paralel. Hal ini memungkinkan program untuk menyimpan hasil perkalian matriks sehingga dapat diakses oleh proses lain.

    Menghasilkan Matriks Acak:
        Program ini menggunakan fungsi rand() untuk menghasilkan angka acak yang akan digunakan sebagai elemen-elemen dalam matriks. Fungsi srand(time(NULL)) digunakan untuk menginisialisasi seed generator angka acak berdasarkan waktu saat ini.

    Inisialisasi dan Pengisian Matriks:
        Terdapat dua matriks yang akan dikalikan, yaitu matriks_satu dengan ukuran ukuran_baris_satu x ukuran_kolom_satu dan matriks_dua dengan ukuran ukuran_baris_dua x ukuran_kolom_dua. Kedua matriks ini diinisialisasi dengan angka acak menggunakan fungsi rand().
        Setelah matriks diinisialisasi, program akan mencetak matriks pertama dan matriks kedua.

    Perkalian Matriks:
        Program melakukan perkalian matriks antara matriks_satu dan matriks_dua dengan menggunakan nested loop.
        Untuk setiap elemen hasil perkalian, hasilnya akan disimpan dalam array hasil_perkalian dan juga disimpan ke dalam shared memory array shr dengan menggunakan variabel c sebagai indeks.

    Cetak Hasil Perkalian:
        Setelah proses perkalian selesai, program akan mencetak hasil perkalian yang tersimpan dalam array shr dengan menggunakan nested loop.

    Membersihkan Shared Memory:
        Setelah selesai menggunakan shared memory, program akan melepaskan akses ke shared memory menggunakan fungsi shmdt((void *)shr).
        Kemudian, program akan menghapus shared memory dengan menggunakan fungsi shmctl(shmid, IPC_RMID, NULL).

Dengan demikian, program kalian.c akan menghasilkan matriks acak, melakukan perkalian matriks, menyimpan hasil perkalian di shared memory, dan mencetak hasil perkalian tersebut.

No 2b:

    Mendeklarasikan dan mengimpor header yang diperlukan, termasuk <stdio.h>, <sys/ipc.h>, <sys/shm.h>, <unistd.h>, <stdlib.h>, dan <pthread.h>.

    Membuat fungsi hitungFaktorial yang akan dijalankan oleh setiap thread. Fungsi ini menerima argumen berupa pointer ke variabel args yang kemudian akan diubah menjadi int dan digunakan sebagai angka untuk menghitung faktorial. Fungsi ini menggunakan variabel-variabel lokal seperti simpan untuk penampung sementara, ukuranArrayNilaiFaktorial untuk menyimpan ukuran array nilai faktorial, dan nilaiFaktorial sebagai array untuk menyimpan digit faktorial.

    Di dalam fungsi hitungFaktorial, faktorial dihitung dengan melakukan iterasi dari 2 hingga angka yang diberikan. Setiap iterasi, digit faktorial dihitung dan disimpan dalam array nilaiFaktorial. Jika hasil perkalian antara digit faktorial dan angka menghasilkan lebih dari satu digit, maka sisanya disimpan dalam variabel simpan. Digit faktorial disimpan secara terbalik dalam array.

    Setelah faktorial dihitung, fungsi hitungFaktorial akan menampilkan hasil faktorial dengan membalik urutan digit pada array nilaiFaktorial dan mencetaknya satu per satu. Selanjutnya, fungsi ini akan mengembalikan NULL.

    Di dalam fungsi main, sebuah kunci dengan nilai 1234 dibuat untuk digunakan dalam shared memory.

    Dilakukan pengalokasian shared memory dengan ukuran 20 * sizeof(int), dan pointer shr diarahkan ke shared memory yang telah dialokasikan.

    Selanjutnya, dilakukan iterasi untuk mencetak hasil perkalian yang disimpan dalam shared memory. Nilai dari setiap elemen shared memory dicetak, dan setiap baris perkalian diakhiri dengan printf("\n").

    Dilakukan penggunaan thread untuk menghitung faktorial dari setiap elemen dalam shared memory. Setiap elemen disimpan dalam variabel number yang dialokasikan menggunakan malloc. Kemudian, sebuah thread dibuat menggunakan pthread_create dengan argumen berupa fungsi hitungFaktorial dan pointer ke number. Setelah thread selesai dieksekusi, hasil faktorialnya dicetak.

    Setelah selesai, dilakukan operasi pemutusan dan penghapusan shared memory menggunakan shmdt dan shmctl secara berurutan.

    Program selesai dan mengembalikan nilai 0.

No 2c:
    Import Library: Pastikan Anda telah mengimpor library yang diperlukan, yaitu stdio.h, sys/ipc.h, sys/shm.h, unistd.h, stdlib.h, dan pthread.h. Library ini akan digunakan untuk fungsi-fungsi dasar input/output, shared memory, threading, dan alokasi memori.

    Membaca Kode: Baca kode secara keseluruhan untuk memahami struktur dan logika program yang akan dijalankan.

    Mendefinisikan Tipe Data dan Fungsi: Perhatikan deklarasi tipe data ull yang digunakan untuk mendefinisikan tipe unsigned long long. Selain itu, ada fungsi hitung_faktorial() yang akan menghitung faktorial dari sebuah angka.

    Membuat Key: Deklarasikan variabel key dengan tipe data key_t dan beri nilainya sesuai kebutuhan. Key ini akan digunakan untuk mengidentifikasi shared memory.

    Membuat Shared Memory: Gunakan fungsi shmget() untuk membuat atau mengakses shared memory. Berikan parameter key, ukuran memori yang dibutuhkan (dalam byte), dan mode akses. Simpan ID shared memory yang dihasilkan dalam variabel shared_memory_id.

    Melampirkan Shared Memory: Gunakan fungsi shmat() untuk melampirkan shared memory ke dalam ruang alamat proses. Berikan parameter shared_memory_id, NULL, dan flag akses. Simpan alamat shared memory yang dikembalikan dalam variabel shared_memory.

    Mengakses dan Menggunakan Shared Memory: Anda dapat melakukan operasi-operasi yang diperlukan pada shared memory. Dalam contoh kode tersebut, kita membaca nilai-nilai dari shared memory dan mencetaknya.

    Menghitung Faktorial: Gunakan fungsi hitung_faktorial() untuk menghitung faktorial dari nilai-nilai yang ada dalam shared memory. Fungsi ini akan mencetak hasil faktorial.

    Selesai: Setelah selesai menggunakan shared memory, gunakan fungsi shmdt() untuk melepaskannya dari ruang alamat proses. Berikan parameter alamat shared memory yang ingin dilepaskan. Kemudian, program akan selesai dengan mengembalikan nilai 0.

    Menjalankan Program: Compile dan jalankan program menggunakan kompilator C yang sesuai.




### **Kendala yang Dialami**
Pada pengerjaan, terdapat kendala teknis yang mengakibatkan Linux stuck di GNU dan wajib melakukan instalasi ulang. Shared memory awalnya tidak terhubung antara soal 2a dengan soal lainnya. Beberapa kali terjadi segmentation fault pada soal 2b dan 2c sehingga tampilan faktorialnya bernilai 0. 

### **Revisi**
Melakukan beberapa perbaikan terutama pada shared memory soal 2b dan 2c supaya bisa terhubung dengan soal 2a. 

<br>

## **Soal 3**

### **Screenshot Hasil**
![WhatsApp_Image_2023-05-13_at_21.33.47](/uploads/596901d6581423b480c48b7f222889e5/WhatsApp_Image_2023-05-13_at_21.33.47.jpg)
### **Cara Pengerjaan**
##### **Fungsi-Fungsi**
- decodePlaylist()
```c
void decodePlaylist(char* path) {
    FILE* fp = fopen(path, "r");
    char method[200];
    char song[200];
    char command[500];
    int count = 0;

    system("echo -n '' > playlist.txt");

    while(fscanf(fp, "%[^\n]", method) != EOF) {
        count++;
        getc(fp);
        if(strcmp(method, "rot13") == 0) {
            fscanf(fp, "%[^\n]", song);
            sprintf(command, "echo \"%s\" | tr 'A-Za-z' 'N-ZA-Mn-za-m' >> playlist.txt", song);
            system(command);
        } else if (strcmp(method, "base64") == 0) {
            fscanf(fp, "%[^\n]", song);
            sprintf(command, "echo \"%s\" | base64 -d >> playlist.txt", song);
            system(command);
            system("echo >> playlist.txt");
        } else {
            fscanf(fp, "%[^\n]", song);
            sprintf(command, "echo \"%s\" | xxd -r -p >> playlist.txt", song);
            system(command);
            system("echo >> playlist.txt");
        }
        getc(fp);
        
        printf("%s %s\n", method, song);
    }
    printf("total: %d\n", count);
}
```
Digunakan untuk mendecode lagu sesuai dengan metode enkripsi yang digunakan

- processPlaylist()
```c
void processPlaylist() {
    pid_t pid = fork();
    if(pid == 0) {
    //     if(sem_trywait(sem) != 0) {
    //         printf("STREAM SYSTEM OVERLOAD\n");
    //         exit(0);
    //     }
        // convert json into (method encoded) format
        system("awk 'BEGIN{FS = \"\\\"\"};/method/{print $4}; /song/{print $4}' song-playlist.json > temp.txt");
        decodePlaylist("temp.txt");
        system("rm temp.txt");
        system("cat playlist.txt | sort -f > temp.txt");
        system("cat temp.txt > playlist.txt");
        system("rm temp.txt");
        // sem_post(sem);
        exit(0);
    }
}
```
Digunakan untuk memanggil fungsi decode dan juga melakukan preprocess JSON agar lebih mudah decode musik. Setelah itu melakukan sort playlist

- play()

```c
void play(int uid, char* name) {
    pid_t pid = fork();
    if(pid > 0) return;
    if(pid < 0) return;
    // if(sem_trywait(sem) != 0) {
    //     printf("STREAM SYSTEM OVERLOAD\n");
    //     exit(0);
    // }
    char command[200];
    char* tempName = strtok(name, "\"");
    sprintf(command, "awk \"BEGIN{IGNORECASE = 1; count = 0} /%s/{arr[count++] = \\$0} END{print count; for (i = 0; i < count; i++) print arr[i]}\" playlist.txt >> temp.txt", tempName);
    system(command);
    FILE* fp = fopen("temp.txt", "r");
    int count;
    for(int i = 0; tempName[i] != '\0'; i++) {
        tempName[i] = toupper(tempName[i]);
    }
    fscanf(fp, "%d\n", &count);
    if(count == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", tempName);
        fclose(fp);
        system("rm temp.txt");
    } else if(count > 1) {
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", count, tempName);
        int i = 1;
        while(fscanf(fp, "%[^\n]", tempName) != EOF) {
            getc(fp);
            printf("%d. %s\n", i, tempName);
        }
        fclose(fp);
        system("rm temp.txt");
    } else {
        fscanf(fp ,"%[^\n]", tempName);
        getc(fp);
        for(int i = 0; tempName[i] != '\0'; i++) {
            tempName[i] = toupper(tempName[i]);
        }
        printf("USER %d PLAYING \"%s\"\n", uid, tempName);
        system("rm temp.txt");
        fclose(fp);
        sleep(5);
    }
    // sem_post(sem);
    exit(0);
}
```
Digunakan untuk memainkan lagu untuk user

- list()

```c
void list(int uid) {
    pid_t pid = fork();
    if(pid > 0) return;
    if(pid < 0) return;

    // if(sem_trywait(sem) != 0) {
    //     printf("STREAM SYSTEM OVERLOAD\n");
    //     exit(0);
    // }
    system("cat playlist.txt");
    // sem_post(sem);
    exit(0);
}
```
Digunakan untuk menampilkan list seluruh playlist

- add()
```c
void add(int uid, char* name) {
    pid_t pid = fork();
    if(pid > 0) return;
    if(pid < 0) return;

    // if(sem_trywait(sem) != 0) {
    //     printf("STREAM SYSTEM OVERLOAD\n");
    //     exit(0);
    // }
    
    char command[200];
    char* tempName = strtok(name, "\"");
    sprintf(command, "awk \"BEGIN{IGNORECASE = 1; found = 0} \\$0 == \\\"%s\\\"{found = 1} END{print found}\" playlist.txt >> temp.txt", tempName);
    system(command);
    
    FILE* fp = fopen("temp.txt", "r");
    int found = 0;
    fscanf(fp, "%d", &found);
    if(found) {
        printf("SONG ALREADY ON PLAYLIST\n");
        system("rm temp.txt");
        fclose(fp);
        // sem_post(sem);
        exit(0);
    }
    sprintf(command, "echo '%s' >> playlist.txt", tempName);
    system(command);
    system("cat playlist.txt | sort -f > temp.txt");
    system("cat temp.txt > playlist.txt");
    system("rm temp.txt");
    printf("USER %d ADD %s\n", uid, tempName);
    // sem_post(sem);
    fclose(fp);
    exit(0);
}
```
Digunakan untuk menambahkan lagu baru ke dalam playlist

#### **Setup Message Queue**
- Pada stream.c
```c
int main()
{
    // key for message queue
    key_t key;
    int msgid;

    // open semaphore
    sem = sem_open("playlist", O_CREAT, 0644, 2);
    sem_init(sem, 1, 2);
  
    // generate unique key
    key = ftok("progfile", 65);
    
  
    // create message queue
    msgid = msgget(key, 0666 | IPC_CREAT);



    while(1) {
        // message loop
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        switch(message.msg_cmd_type) {
        case PLAY_CMD:
            play(message.msg_uid, message.msg_text);
            break;
        case LIST_CMD:
            list(message.msg_uid);
            break;
        case ADD_CMD:
            add(message.msg_uid, message.msg_text);
            break;
        case DECRYPT_CMD:
            processPlaylist();
            break;
        case CON_CMD:
            message.msg_type = 2;
            strcpy(message.msg_text, "success");
            if(sem_trywait(sem) != 0) {
                message.msg_uid = -1;
                msgsnd(msgid, &message, sizeof(message), 0);
                message.msg_type = 1;
                break;
            }
            msgsnd(msgid, &message, sizeof(message), 0);
            strcpy(message.msg_text, " ");
            message.msg_type = 1;
            break;
        default:
            printf("UNKNOWN COMMAND\n");
        }
    }
    // destryoy message queue
    msgctl(msgid, IPC_RMID, NULL);

    return 0;
}
```
Message reading loop yang akan memilih msg_type 1 (penerima stream.c) yang dibagi menjadi beberapa command. untuk CON_CMD atau connect command akan melakukan decrement semaphore sebagai alat untuk limit user yang mengakses playlist

- Pada user.c
```c
int main()
{
    // key for message queue
    key_t key;
    int msgid;
    int user_id;
    char command[100];
    char song[100];
    
    
  
    // generate unique key
    key = ftok("progfile", 65);
  
    // create message queue
    msgid = msgget(key, 0666 | IPC_CREAT);

    while(1) {
        printf("Enter user id: ");
        scanf("%d", &user_id);
        
        message.msg_uid = user_id;
        
        message.msg_type = 1;
        message.msg_cmd_type = 5;
        strcpy(message.msg_text, " ");
        
        
        msgsnd(msgid, &message, sizeof(message), 0);
        msgrcv(msgid, &message, sizeof(message), 2, 0);
        
        if(message.msg_uid < 0) {
            printf("STREAM SYSTEM OVERLOAD!\n");
            continue;
        } else break;
    }

    // set message type for msgrcv
    message.msg_type = 1;
    
    while(1) {
        scanf("%s", command);
        if(strcmp(command, "PLAY") == 0) {
            message.msg_cmd_type = 1;
            scanf(" %[^\n]", song);
            getchar();
            strcpy(message.msg_text, song);
            
        } else if(strcmp(command, "LIST") == 0) {
            message.msg_cmd_type = 2;
            // msgsnd(msgid, &message, sizeof(message), 0);
        } else if(strcmp(command, "ADD") == 0) {
            message.msg_cmd_type = 3;
            scanf(" %[^\n]", song);
            getchar();
            strcpy(message.msg_text, song);
            // msgsnd(msgid, &message, sizeof(message), 0);
        } else if(strcmp(command, "DECRYPT") == 0) {
            message.msg_cmd_type = 4;
        } else {
            message.msg_cmd_type = -1;
        }
        msgsnd(msgid, &message, sizeof(message), 0);
    }
    return 0;
}
```
Pada user.c hanya mengirim command yang diindikasikan dengan msg_cmd_type jika command tidak diketahui akan mengirim msg_cmd_type -1 ke stream.c dan output UNKNOWN COMMAND

### **Kendala yang Dialami**
Tidak ada kendala yang dialami saat pengerjaan soal ini
### **Revisi**
Pada pengerjaan pertama semaphore digunakan untuk pembatasan playlist.txt, sudah diperbaiki untuk pembatasan stream.c
<br>

## **Soal 4**

### **Screenshot Hasil**

Folder **soal4** setelah run **unzip.c** :

![soal4_unzip](/uploads/ee32ff80781954092352734e3287f73a/image.png)

Isi folder **files** sebelum run **categorize.c** :

![files_unzip](/uploads/7a4e996355008f82b247d296b43345c3/image.png)

**Terminal** setelah run **categorize.c** :

![terminal_categorize](/uploads/10cf7ac217a737b324304b832ef597a5/image.png)

Folder **soal4** setelah run **categorize.c** :

![soal4_categorize](/uploads/c184e7e678f85b2a4ff7ff94e25a4d5a/image.png)

Folder **files** setelah run **categorize.c** :

![files_categorize](/uploads/ddbd4c1f8bb6cec6b3bbf73a11252014/image.png)

Folder **categorized** setelah run **categorize.c** :

![categorized_categorize](/uploads/21ee248a5d28e8323eb4dfd1e65f77f1/image.png)

**Terminal** setelah run **logchecker.c** :

![terminal_logchecker](/uploads/aea9020920968ce683666a64be1af2fe/image.png)

### **Cara Pengerjaan**

#### **unzip.c**

> Download dan unzip file [hehe.zip](https://drive.google.com/file/d/1Fxu7L83m1qDUM84fvsrQN3iwEjaxeRLEy_0fqp/view) dalam kode c bernama unzip.c

Berikut adalah function untuk melakukan download file .zip yang diminta pada soal :

```c
void download()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan download\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char link[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
        char *argv[] = {"wget", "-q", link, "-O", "hehe.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses download tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}
```

- Function di atas melakukan download dengan menggunakan `fork()`.
- Proses download dijalankan pada *child process* dengan menggunakan command `wget`.
- Command `wget` dijalankan dengan menggunakan function `execv()`.

<br>

Berikut adalah function untuk melakukan unzip pada file .zip yang sudah didownload :

```c
void unzip()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan unzip pada hehe.zip\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"unzip", "-q", "hehe.zip", NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses unzip pada hehe.zip tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}
```

- Function di atas melakukan unzip dengan menggunakan `fork()`.
- Proses unzip dijalankan pada *child process* dengan menggunakan command `unzip`.
- Command `unzip` dijalankan dengan menggunakan function `execv()`.

<br>

Berikut adalah potongan kode yang dijalankan pada function `main()` :

```c
int main()
{
    download();
    unzip();
    return 0;
}
```

- Download file .zip yang diminta pada soal dengan memanggil function `download()`.
- Unzip file yang sudah didownload dengan memanggil function `unzip()`.

#### **categorize.c**

> Selanjutnya, buatlah program **categorize.c** untuk mengumpulkan (move / copy) *file* sesuai *extension*-nya. *Extension* yang ingin dikumpulkan terdapat dalam *file* **extensions.txt**. Buatlah folder dengan nama sesuai nama *extension*-nya dengan nama *folder* semua *lowercase*. Akaa tetapi, *file* bisa saja tidak semua *lowercase*. *File* lain dengan *extension* selain yang terdapat dalam .txt *files* tersebut akan dimasukkan ke *folder* **other**.
> 
> Pada *file **max.txt**, terdapat angka yang merupakan isi maksimum dari *folder* tiap *extension* kecuali *folder* **other**. Sehingga, jika penuh, buatlah *folder* baru dengan **extension (2)**, **extension (3)**, dan seterusnya.
> *Output*-kan pada *terminal* banyaknya *file* tiap *extension* terurut *ascending* dengan semua *lowercase*, beserta **other** juga dengan format sebagai berikut :
>
> **extension_a : banyak_file**
> **extension_b : banyak_file**
> **extension_c : banyak_file**
> **other : banyak_file**
>
> Setiap pengaksesan *folder*, *sub-folder*, dan semua *folder* pada program *categorize.c* **wajib menggunakan multithreading**.
>
> Dalam setiap pengaksesan *folder*, pemindahan *file*, pembuatan *folder* pada program categorize.c buatlah *log* dengan format sebagai berikut :
> 
> **DD-MM-YYYY HH:MM:SS ACCESSED [folder path]**
> **DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]**
> **DD-MM-YYYY HH:MM:SS MADE [folder name]**
>
> Catatan :
> - *Path* dimulai dari *folder files* atau *categorized*
> - Simpan di dalam **log.txt**
> - ACCESSED merupakan folder files beserta dalamnya
> - Urutan log tidak harus sama

Berikut adalah struct yang digunakan untuk menyimpan nama suatu extension serta banyaknya file yang memiliki extension tersebut :

```c
typedef struct Extension
{
    char name[10];
    int ct_file;
}
Ext;
```

<br>

Berikut adalah beberapa variabel global yang nantinya akan digunakan pada program :

```c
Ext extList[100];
int ct_ext;
int ct_other;
int max_folder_size;
```

- `extList :` list extension yang ada pada `"extensions.txt"`, berupa array of struct `Extension`.
- `ct_ext :` banyaknya extension yang ada pada `"extensions.txt"`.
- `ct_other :` banyaknya file yang masuk ke dalam folder `other`.
- `max_folder_size :` maksimum banyaknya file dalam suatu folder pada direktori `categorized/`, nilai `max_folder_size` dibaca dari file `"max.txt"`.

<br>

Berikut adalah function untuk mendapatkan *timestamp* saat ini :

```c
char *get_timestamp()
{
    time_t now = time(NULL);
    struct tm *tm_info = localtime(&now);
    char *timestamp = (char *) malloc(20 * sizeof(char));

    strftime(timestamp, 20, "%d-%m-%Y %H:%M:%S", tm_info);
    return timestamp;
}
```

- Mendapatkan `raw time` saat ini dengan menggunakan function `localtime()`
- Alokasi memori untuk string `timestamp` dengan menggunakan function `malloc()`. Apabila string `timestamp` dideklarasikan dengan biasa (`char timestamp[20]`) maka akan terjadi error pada memori.
- Sesuaikan format waktu dengan yang diminta pada soal menggunakan function `strftime()`.
- Kembalikan string `timestamp`.

<br>

Berikut adalah function untuk menulis log dengan format sesuai pada permintaan soal ke file `"log.txt"` :

```c
void write_log(char *log)
{
    FILE *fp;
    fp = fopen("log.txt", "a");

    if(fp == NULL){
        printf("Gagal membuka file log.txt\n");
        exit(1);
    }
    fprintf(fp, "%s %s\n", get_timestamp(), log);
    fclose(fp);
}
```

- Function menerima argument berupa string `log`, yaitu jenis dari `log` tersebut (`"MADE", "ACCESSED", "MOVED"`) serta apa yang dilakukan.
- Membuka file `"log.txt"` untuk melakukan *append*.
- *Append* `timestamp` serta `log` pada file `"log.txt"`.

<br>

Berikut adalah function untuk membuat folder baru :

```c
void make_dir(char *dirname)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat folder %s\n", dirname);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan folder %s tidak berhenti dengan normal\n", dirname);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MADE %s", dirname + 2);
    write_log(log);
}
```

- Function menerima argument berupa direktori lengkap dari folder baru yang akan dibuat. Direktori dimulai dari `categorized/` atau `files/`.
- Proses pembuatan folder dijalankan pada *child process* dengan menggunakan `fork()`.
- Function yang digunakan untuk menjalankan proses membuat folder adalah `execv()`, dengan menggunakan perintah `mkdir`.
- Setelah berhasil membuat folder, dilakukan penulisan log jenis `"MADE"` pada *parent process*.

<br>

Berikut adalah function untuk membuat file baru :

```c
void make_file(char *filename)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"touch", filename, NULL};
        execv("/usr/bin/touch", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
}
```

- Function menerima argument berupa nama file baru yang akan dibuat.
- Proses pembuatan file dijalankan pada *child process* dengan menggunakan `fork()`.
- Function yang digunakan untuk menjalankan proses membuat file adalah `execv()`, dengan menggunakan perintah `touch`.

<br>

Berikut adalah function untuk memindahkan file :

```c
void move_file(char *filename, char *dest, char *ext)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal memindahkan file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mv", filename, dest, NULL};
        execv("/usr/bin/mv", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pemindahan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MOVED %s file : %s > %s", ext, filename, dest + 2);
    write_log(log);
}
```

- Function menerima argument berupa nama file yang akan dipindahkan, *directory path* lengkap dari tujuan pemindahan, serta extension dari file yang akan dipindah. Direktori dimulai dari `categorized/` atau `files/`.
- Proses pemindahan file dijalankan pada *child process* dengan menggunakan `fork()`.
- Function yang digunakan untuk menjalankan proses membuat folder adalah `execv()`, dengan menggunakan perintah `mv`.
- Setelah berhasil membuat folder, dilakukan penulisan log jenis `"MOVED"` pada *parent process*.

<br>

Berikut adalah function untuk mengecek apakah suatu file memiliki extension sesuai dengan yang diminta :

```c
bool check_extension(char *filename, char *ext)
{
    int len_file = strlen(filename);
    int len_ext = strlen(ext);

    if(len_file <= len_ext){
        return false;
    }
    char temp[len_ext + 1];
    strcpy(temp, filename + len_file - len_ext);

    for(int i = 0; i < len_ext; ++i){
        if(isalpha(temp[i]))
            temp[i] = tolower(temp[i]);
    }
    return (strcmp(temp, ext) == 0);
}
```

- Function menerima argument berupa nama file serta extension yang akan dicek. Apabila file `filename` memiliki extension `ext`, maka function akan mengembalikan nilai `true`, jika sebaliknya maka function akan mengembalikan nilai `false`.
- Kasus yang sudah pasti `false` adalah jika panjang nama file <= panjang nama extension.
- Extension pada nama file yang akan dicek diubah dahulu menjadi huruf kecil semua, karena pada soal ini terdapat kemungkinan extension file memiliki huruf kapital.
- Kembalikan nilai `true` apabila extension dari file sesuai dengan yang dicek, kembalikan nilai `false` jika sebaliknya.

<br>

Berikut adalah function utama dalam proses *categorizing* file-file yang ada :

```c
void *categorizing(void *args)
{
    char *path = (char *) args;
    DIR *dir = opendir(path);
    struct dirent *ent;

    if(dir == NULL){
        printf("Gagal membuka direktori %s\n", path);
        pthread_exit(NULL);
    }
    while((ent = readdir(dir)) != NULL)
    {
        if(strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0){
            continue;
        }
        if(ent->d_type == DT_DIR)
        {
            char sub_dir[1000], log[2000];
            sprintf(sub_dir, "%s/%s", path, ent->d_name);

            sprintf(log, "ACCESSED %s", sub_dir);
            write_log(log);

            pthread_t thread;
            pthread_create(&thread, NULL, categorizing, (void *) sub_dir);
            pthread_join(thread, NULL);
        }
        else if(ent->d_type == DT_REG)
        {
            bool other = true;
            char file_path[1000];
            sprintf(file_path, "%s/%s", path, ent->d_name);

            for(int i = 0; i < ct_ext; ++i)
            {
                if(check_extension(ent->d_name, extList[i].name))
                {
                    char dest[1000];
                    extList[i].ct_file++;
                    int idx_folder = floor(extList[i].ct_file / max_folder_size);

                    if(idx_folder == 0)
                        sprintf(dest, "./categorized/%s", extList[i].name);
                    else {
                        sprintf(dest, "./categorized/%s (%d)", extList[i].name, idx_folder + 1);
                        if(access(dest, F_OK) != 0)
                            make_dir(dest);
                    }
                    move_file(file_path, dest, extList[i].name);
                    other = false;
                    break;
                }
            }
            if(other){
                move_file(file_path, "./categorized/other", "other");
                ct_other++;
            }
        }
    }
    closedir(dir);
    pthread_exit(NULL);
}
```

- Function ini dijalankan dengan menggunakan *multi threading*.
- Argument berupa *directory path* saat ini. Argument awal saat pemanggilan dari `main()` adalah `files`, sehingga seluruh proses *traversing* dimulai dari direktori `files/`.
- Proses *traversing* seluruh folder, sub-folder, dan file menggunakan `DIR` dan `struct dirent`.
- Untuk membaca seluruh isi dari sebuah direktori, bisa menggunakan loop `while((ent = readdir(dir)) != NULL)`, dengan `ent` adalah *entry* atau isi yang dibaca dari direktori saat ini, baik itu berupa file, folder, dan lain-lain.
- `DT_DIR` menandakan bahwa jenis *entry* yang sedang dibaca merupakan sebuah folder.
- Jika *entry* berupa folder, maka *traverse* folder tersebut dengan cara memanggil function `categorizing()` secara rekursif menggunakan *multi threading*, serta tulis log dengan jenis `"ACCESSED"`.
- `DT_REG` menandakan bahwa jenis *entry* yang sedang dibaca merupakan sebuah *regular file*.
- Jika *entry* berupa file, maka lakukan pengecekan extension dari file tersebut.
- Jika ditemukan extension yang cocok, maka pindahkan file tersebut ke folder yang cocok di dalam direktori `categorized/`.
- Apabila folder yang dituju tidak ditemukan atau sudah penuh, maka buat folder baru dengan memanggil function `make_dir()`.
- Jika tidak ditemukan extension yang cocok, maka pindahkan file tersebut ke `categorized/other`.
- Pemindahan file dilakukan dengan memanggil function `move_file()`.

<br>

Berikut adalah function untuk membaca file `"extensions.txt"` dan `"max.txt"` :

```c
void read_files()
{
    char buffer[10];
    FILE *fp;
    fp = fopen("extensions.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file extensions.txt\n");
        exit(1);
    }
    while(fgets(buffer, 10, fp) != NULL)
    {
        if(buffer[strlen(buffer) - 1] == '\n')
            buffer[strlen(buffer) - 1] = '\0';
        strcpy(extList[ct_ext].name, buffer);
        ct_ext++;
    }
    for(int i = 0; i < ct_ext; ++i){
        extList[i].name[strlen(extList[i].name) - 1] = '\0';
    }
    fp = fopen("max.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file max.txt\n");
        exit(1);
    }
    while(fgets(buffer, 10, fp) != NULL)
        max_folder_size = atoi(buffer);
    fclose(fp);
}
```

- List extension yang dibaca pada file `"extensions.txt"` disimpan di dalam `extList`.
- Banyaknya file maksimum dari suatu folder pada file `"max.txt"` disimpan di dalam `max_folder_size`.

<br>

Berikut adalah function untuk membuat folder `categorized`, `categorized/other`, serta seluruh folder extension dari `extList` :

```c
void make_folders()
{
    make_dir("./categorized");
    for(int i = 0; i < ct_ext; ++i){
        char path[1000];
        sprintf(path, "./categorized/%s", extList[i].name);
        make_dir(path);
    }
    make_dir("./categorized/other");
}
```

<br>

Berikut adalah function untuk menukar 2 struct `Extension` :

```c
void swap_Ext(Ext *a, Ext *b)
{
    Ext temp = *a;
    *a = *b;
    *b = temp;
}
```

<br>

Berikut adalah function untuk mengurutkan extension secara *ascending* berdasarkan banyaknya file yang memiliki extension tersebut :

```c
void sort_Ext()
{
    for(int j = ct_ext - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(extList[i].ct_file > extList[i + 1].ct_file)
                swap_Ext(extList + i, extList + i + 1);
        }
    }
}
```

- Function di atas memanfaatkan algoritma `Bubble Sort`.

<br>

Berikut adalah function untuk menampilkan banyaknya file dari masing-masing extension serta dari folder `other` :

```c
void print_Ext()
{
    for(int i = 0; i < ct_ext; ++i)
        printf("extension_%-5s : %d\n", extList[i].name, extList[i].ct_file);
    printf("%-15s : %d\n", "other", ct_other);
}
```

<br>

Berikut adalah potongan kode dari function `main()` :

```c
int main()
{
    read_files();
    make_folders();
    make_file("log.txt");

    char path[1000] = "files";
    pthread_t thread;
    pthread_create(&thread, NULL, categorizing, (void *) path);
    pthread_join(thread, NULL);

    sort_Ext();
    print_Ext();
    return 0;
}
```

- Baca file `"extensions.txt"` dan `"max.txt"` dengan memanggil function `read_files()`.
- Buat folder `categorized`, `other`, serta seluruh folder extension dengan memanggil function `make_folders()`.
- Buat file `"log.txt"` dengan memanggil function `make_file()`.
- Memulai proses *categorizing* dengan memanggil function `categorizing` dengan argument `"files"` menggunakan *multi threading*.
- Lakukan *sorting* pada `extList` secara *ascending* berdasarkan banyak file dengan memanggil function `sort_Ext()`.
- Tampilkan banyak file masing-masing extension dengan memanggil `print_Ext()`.

#### **logchecker.c**

> Untuk mengecek apakah *log*-nya benar, buatlah suatu program baru dengan nama **logchecker.c** untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut :
> - Untuk menghitung banyaknya ACCESSED yang dilakukan
> - Untuk membuat *list* **seluruh folder** yang telah dibuat dan banyaknya *file* yang dikumpulkan ke folder tersebut, terurut secara *ascending*.
> - Untuk menghitung banyaknya total *file* tiap *extension*, terurut secara *ascending*.

Berikut adalah variabel global yang nantinya akan digunakan di dalam program :

```c
int ct_acc;
int ct_fd;
int ct_ext;
```

- `ct_acc :` menyimpan banyaknya log `"ACCESSED"`.
- `ct_fd :` menyimpan banyaknya folder
- `ct_ext :` menyimpan banyaknya extension

<br>

Berikut adalah struct untuk menyimpan nama dan banyaknya file pada suatu extension atau folder :

```c
typedef struct Folder
{
    char name[1000];
    int ct_file;
}
FD;

typedef struct Extension
{
    char name[10];
    int ct_file;
}
Ext;
```

<br>

Berikut adalah function untuk menukar 2 folder atau extension :

```c
void swap_FD(FD *a, FD *b)
{
    FD temp = *a;
    *a = *b;
    *b = temp;
}

void swap_Ext(Ext *a, Ext *b)
{
    Ext temp = *a;
    *a = *b;
    *b = temp;
}
```

<br>

Berikut adalah function untuk *sorting* folder dan extension berdasarkan banyak file secara *ascending* menggunakan `Bubble Sort` :

```c
void sortAll(FD *fdList, Ext *extList)
{
    for(int j = ct_fd - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(fdList[i].ct_file > fdList[i + 1].ct_file)
                swap_FD(fdList + i, fdList + i + 1);
        }
    }
    for(int j = ct_ext - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(extList[i].ct_file > extList[i + 1].ct_file)
                swap_Ext(extList + i, extList + i + 1);
        }
    }
}
```

<br>

Berikut adalah function untuk menampilkan list seluruh folder dan extension beserta banyak file masing-masing :

```c
void printAll(FD *fdList, Ext *extList)
{
    printf("TOTAL ACCESSED : %d\n\n", ct_acc);

    printf("--- LIST FOLDER ---\n");
    for(int i = 0; i < ct_fd; ++i)
        printf("%-19s : %d\n", fdList[i].name, fdList[i].ct_file);
    printf("\n");

    printf("--- LIST EXTENSION ---\n");
    for(int i = 0; i < ct_ext; ++i)
        printf("%-5s : %d\n", extList[i].name, extList[i].ct_file);
    printf("\n");
}
```

<br>

Berikut adalah function untuk membaca log :

```c
void read_log(FD *fdList, Ext *extList)
{
    char buffer[2000];
    FILE *fp;
    fp = fopen("log.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file log.txt\n");
        exit(1);
    }
    while(fgets(buffer, 1000, fp) != NULL)
    {
        if(strstr(buffer, " ACCESSED ") != NULL)
            ct_acc++;
        else if(strstr(buffer, " MADE ") != NULL)
        {
            char fd[1000];
            sscanf(buffer, "%*s %*s %*s %[^\n]s", fd);
            fd[strlen(fd)] = '\0';

            int i;
            for(i = 0; i < ct_fd; ++i){
                if(strcmp(fd, fdList[i].name) == 0)
                    break;
            }
            if(i == ct_fd){
                strcpy(fdList[i].name, fd);
                fdList[i].ct_file = 0;
                ct_fd++;
            }
        }
        else if(strstr(buffer, " MOVED ") != NULL)
        {
            char ext[10], fd[1000];
            sscanf(buffer, "%*s %*s %*s %s", ext);

            char *temp = strstr(buffer, " > ");
            strcpy(fd, temp + 3);
            fd[strlen(fd) - 1] = '\0';

            int i;
            for(i = 0; i < ct_ext; ++i){
                if(strcmp(ext, extList[i].name) == 0){
                    extList[i].ct_file++;
                    break;
                }
            }
            if(i == ct_ext){
                strcpy(extList[i].name, ext);
                extList[i].ct_file = 1;
                ct_ext++;
            }
            for(i = 0; i < ct_fd; ++i){
                if(strcmp(fd, fdList[i].name) == 0){
                    fdList[i].ct_file++;
                    break;
                }
            }
        }
    }
    fclose(fp);
}
```

- Buka dan baca file `"log.txt"`.
- Jika log merupakan `"ACCESSED"`, maka *increment* nilai dari `ct_acc`.
- Jika log merupakan `"MADE"`, cari apakah folder tersebut sudah pernah dibuat, jika belum maka tambahkan ke dalam `fdList` dan *increment* nilai dari `ct_fd`.
- Jika log merupakan `"MOVED"`, cari extension yang sesuai dari file tersebut dan *increment* banyak file dari extension tersebut. Jika tidak ditemukan extension yang sesuai, maka tambahkan extension tersebut ke dalam `extList` serta *increment* `ct_ext` dan banyak file dari extension tersebut. Kemudian, cari folder yang sesuai dari file tersebut dan *increment* banyak file dari folder tersebut.

<br>

Berikut adalah potongan kode dari function `main()` :

```c
int main()
{
    FD fdList[400];
    Ext extList[100];
    read_log(fdList, extList);
    sortAll(fdList, extList);
    printAll(fdList, extList);
    return 0;
}

```

- Buat `fdList` untuk menyimpan list folder dan `extList` untuk menyimpan list extension.
- Baca log dengan memanggil function `read_log()`.
- Urutkan `fdList` dan `extList` secara *ascending* berdasarkan banyak file dengan memanggil function `sortAll()`.
- Tampilkan seluruh list folder dan extension beserta banyak file masing-masing dengan memanggil function `printAll()`.

### **Kendala yang Dialami**

Belum sempat mengerjakan soal 4 saat praktikum :'(

### **Revisi**

Berhasil mengerjakan soal 4 saat revisi :)
