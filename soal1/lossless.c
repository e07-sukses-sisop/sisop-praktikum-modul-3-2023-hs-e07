#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

// Heap yang digunakan pada program ini adalah Min Heap
// yaitu dengan top memiliki value minimum
typedef struct MinHeapNode
{
    char key;
    int val;
    struct MinHeapNode *left, *right;
}
HNode;

typedef struct MinHeap
{
    unsigned size, capacity;
    HNode **arr;
}
Heap;

HNode *createNode(char key, int val)
{
    HNode *newNode = (HNode *) malloc(sizeof(HNode));
    newNode->key = key;
    newNode->val = val;
    newNode->left = newNode->right = NULL;
    return newNode;
}

Heap *createHeap(int capacity)
{
    Heap *newHeap = (Heap *) malloc(sizeof(Heap));
    newHeap->size = 0;
    newHeap->capacity = capacity;
    newHeap->arr = (HNode **) malloc(capacity * sizeof(HNode *));
    return newHeap;
}

void swapNode(HNode **a, HNode **b)
{
    HNode *temp = *a;
    *a = *b;
    *b = temp;
}

void heapify(Heap *heap, int idx)
{
    int min = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if(left < heap->size && heap->arr[left]->val < heap->arr[min]->val)
        min = left;
    if(right < heap->size && heap->arr[right]->val < heap->arr[min]->val)
        min = right;
    if(min != idx)
    {
        swapNode(&heap->arr[idx], &heap->arr[min]);
        heapify(heap, min);
    }
}

HNode *getMin(Heap *heap)
{
    HNode *temp = heap->arr[0];
    heap->arr[0] = heap->arr[heap->size - 1];

    heap->size--;
    heapify(heap, 0);
    return temp;
}

void insertNode(Heap *heap, HNode *node)
{
    heap->size++;
    int cur = heap->size - 1;
    int par = (cur - 1) / 2;

    while(cur > 0 && node->val < heap->arr[par]->val)
    {
        heap->arr[cur] = heap->arr[par];
        cur = par;
        par = (cur - 1) / 2;
    }
    heap->arr[cur] = node;
}

void buildHeap(Heap *heap)
{
    for (int i = (heap->size - 1) / 2; i >= 0; --i)
        heapify(heap, i);
}

Heap *initHeap(char key[], int val[], int size)
{
    Heap *heap = createHeap(size);
    for (int i = 0; i < size; ++i)
        heap->arr[i] = createNode(key[i], val[i]);

    heap->size = size;
    buildHeap(heap);
    return heap;
}

// Tree yang digunakan pada program ini adalah Huffman Tree
// yaitu Tree untuk lossless data compression, dengan
// memanfaatkan frekuensi kemunculan character
HNode *buildTree(char key[], int val[], int size)
{
    HNode *left, *right, *par;
    Heap *heap = initHeap(key, val, size);

    while(heap->size > 1)
    {
        left = getMin(heap);
        right = getMin(heap);
        par = createNode('*', left->val + right->val);

        par->left = left;
        par->right = right;
        insertNode(heap, par);
    }
    return getMin(heap);
}

int key_idx;
void encode(int bit[], int digit, char code[][50])
{
    for (int i = 0; i < digit; ++i)
        code[key_idx][i] = '0' + bit[i];
}

void getCode(HNode *node, int bit[], int depth, char code[][50], char codeKey[])
{
    if(node->left){
        bit[depth] = 0;
        getCode(node->left, bit, depth + 1, code, codeKey);
    }
    if(node->right){
        bit[depth] = 1;
        getCode(node->right, bit, depth + 1, code, codeKey);
    }
    if (!node->left && !node->right)
    {
        codeKey[key_idx] = node->key;
        encode(bit, depth, code);
        key_idx++;
    }
}

HNode *Huffman(char key[], int val[], int size, char code[][50], char codeKey[])
{
    HNode *node = buildTree(key, val, size);
    int bit[50], depth = 0;
    getCode(node, bit, depth, code, codeKey);
    return node;
}

typedef struct HeapData
{
    char key;
    int val;
}
HData;

void preorder(HNode *node, HData newHeap[], int idx)
{
    newHeap[idx].key = node->key;
    newHeap[idx].val = node->val;

    if(isalpha(node->key))
        return;
    if(node->left)
        preorder(node->left, newHeap, 2 * idx + 1);
    if(node->right)
        preorder(node->right, newHeap, 2 * idx + 2);
}

int main()
{
    int fd1[2]; // kirim hasil perhitungan frekuensi (parent ke child)
    int fd2[2]; // kirim hasil kompresi (child ke parent)

    if(pipe(fd1) == -1){
        fprintf(stderr, "Gagal membuat Pipe 1\n");
        return 1;
    }
    if(pipe(fd2) == -1){
        fprintf(stderr, "Gagal membuat Pipe 2\n");
        return 1;
    }
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        fprintf(stderr, "Gagal membuat Fork\n");
        exit(EXIT_FAILURE);
    }
    else if(child_id > 0)   // parent
    {
        /* 1A */
        char filename[] = "file.txt";
        char str[1000];

        int count[30], ct_alpha = 0;
        memset(count, 0, sizeof(count));

        FILE *fp;
        fp = fopen(filename, "r");

        if(fp == NULL){
            printf("Gagal membuka file %s\n", filename);
            exit(1);
        }
        // Perhitungan Frekuensi
        while(fgets(str, 1000, fp) != NULL)
        {
            for (int i = 0; str[i]; ++i){
                if(isalpha(str[i])){
                    count[toupper(str[i]) - 'A']++;
                    ct_alpha++;
                }
            }
        }
        fclose(fp);

        // Tulis hasil perhitungan frekuensi ke pipe 1
        close(fd1[0]);
        write(fd1[1], count, sizeof(count));
        close(fd1[1]);

        /* 1D */
        wait(NULL);
        char codeKey[30];
        char code[30][50];
        char compressed[4000];
        char newSize_str[10];
        int newSize = 0;

        // Membaca hasil kompresi dari pipe 2
        close(fd2[1]);
        read(fd2[0], codeKey, sizeof(codeKey));
        read(fd2[0], code, sizeof(code));
        read(fd2[0], compressed, sizeof(compressed));
        read(fd2[0], newSize_str, sizeof(newSize_str));
        
        newSize = atoi(newSize_str);
        HData newHeap[newSize];

        read(fd2[0], newHeap, sizeof(newHeap));
        close(fd2[0]);

        int len = strlen(compressed);
        int idx = 0;
        printf("Hasil Dekompresi :\n");

        for (int i = 0; i < len; ++i)
        {
            if(isalpha(newHeap[idx].key)){
                printf("%c", newHeap[idx].key);
                idx = 0;
            }
            if(compressed[i] == '0')
                idx = 2 * idx + 1;
            else
                idx = 2 * idx + 2;
        }
        printf("%c\n\n", newHeap[idx].key);

        /* 1E */
        printf("Total bit sebelum kompresi : %d\n", ct_alpha * 8);
        printf("Total bit setelah kompresi : %d\n", len);

        exit(0);
    }
    else {  // child
        /* 1B */
        int count[30], size = 0;
        char key[30];

        // Membaca hasil perhitungan frekuensi dari pipe 1
        close(fd1[1]);
        read(fd1[0], count, sizeof(count));
        close(fd1[0]);

        // Membuat Leaf dari Huffman Tree
        for(int i = 0; i < 26; ++i){
            if(count[i] > 0){
                key[size] = 'A' + i;
                count[size++] = count[i];
            }
        }
        char code[30][50], codeKey[30];
        HNode *root = Huffman(key, count, size, code, codeKey);

        /* 1C */
        int max_depth = 0;
        for(int i = 0; i < size; ++i){
            if(strlen(code[i]) > max_depth)
                max_depth = strlen(code[i]);
        }
        int newSize = (1 << (max_depth + 1)) - 1;
        char newSize_str[10] = "";
        sprintf(newSize_str, "%d", newSize);

        HData newHeap[newSize];
        for(int i = 0; i < newSize; ++i){
            newHeap[i].key = ' ';
            newHeap[i].val = 0;
        }
        preorder(root, newHeap, 0);

        // Lakukan Kompresi
        char filename[] = "file.txt";
        char str[1000];
        char compressed[4000] = "";
        FILE *fp;
        fp = fopen(filename, "r");

        if(fp == NULL){
            printf("Gagal membuka file %s\n", filename);
            exit(1);
        }
        while(fgets(str, 1000, fp) != NULL)
        {
            for (int i = 0; str[i]; ++i)
            {
                if(isalpha(str[i])){
                    str[i] = toupper(str[i]);

                    for(int j = 0; j < strlen(codeKey); ++j){
                        if(str[i] == codeKey[j]){
                            strcat(compressed, code[j]);
                            break;
                        }
                    }
                }
            }
        }
        fclose(fp);

        // Kirim hasil kompresi ke pipe 2
        close(fd2[0]);
        write(fd2[1], codeKey, sizeof(codeKey));
        write(fd2[1], code, sizeof(code));
        write(fd2[1], compressed, sizeof(compressed));

        write(fd2[1], newSize_str, sizeof(newSize_str));
        write(fd2[1], newHeap, sizeof(newHeap));
        close(fd2[1]);

        exit(0);
    }
    return 0;
}
