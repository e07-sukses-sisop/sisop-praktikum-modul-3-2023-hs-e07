#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

void *hitungFaktorial(void *args) {
    int angka = *((int *)args);
    int simpan = 0; // Penampung sementara
    int ukuranArrayNilaiFaktorial = 0; // Ukuran array nilai faktorial
    int nilaiFaktorial[50]; // Array nilai faktorial

    nilaiFaktorial[0] = 1;

    for (int i = 2; i <= angka; i++) {
        simpan = 0;
        // Menghitung faktorial dan menyimpan digit terakhir, jika lebih dari satu digit maka sisanya disimpan di variabel simpan
        for (int j = 0; j <= ukuranArrayNilaiFaktorial; j++) {
            simpan += nilaiFaktorial[j] * i;
            nilaiFaktorial[j] = simpan % 10;
            simpan /= 10;
        }

        // Menyimpan sisa digit jika ada
        while (simpan > 0) {
            ukuranArrayNilaiFaktorial++;
            nilaiFaktorial[ukuranArrayNilaiFaktorial] = simpan % 10;
            simpan /= 10;
        }
    }

    // Menampilkan hasil faktorial
    for (int i = ukuranArrayNilaiFaktorial; i >= 0; i--) {
        printf("%d", nilaiFaktorial[i]);
    }
    printf(" ");
    return NULL;
}

int main() {
    key_t kunci = 1234;
    int *shr;

    int shmid = shmget(kunci, 20 * sizeof(int), IPC_CREAT | 0666);
    shr = (int *)shmat(shmid, NULL, 0);

    int indeks = 0;
    printf("Hasil Perkalian:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", shr[indeks]);
            indeks++;
        }
        printf("\n");
    }

    pthread_t t_id[20];

    printf("Hasil Faktorial:\n");
    for (int i = 0; i < 20; i++) {
        int *number = (int *)malloc(sizeof(int));
        *number = shr[i];
        pthread_create(&t_id[i], NULL, &hitungFaktorial, (void *)number);

        pthread_join(t_id[i], NULL);
        if ((i + 1) % 5 == 0)
            printf("\n");
    }

    shmdt((void *)shr);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
