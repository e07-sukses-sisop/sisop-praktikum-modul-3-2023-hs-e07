// kalian.c
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define ukuran_baris_satu 4
#define ukuran_kolom_satu 2
#define ukuran_baris_dua 2
#define ukuran_kolom_dua 5

int main() {
    key_t key = 1234;
    srand(time(NULL));
    
    int *shr;

    int shmid = shmget(key, (ukuran_baris_satu * ukuran_kolom_satu + ukuran_baris_dua * ukuran_kolom_dua) * sizeof(int), IPC_CREAT | 0666);

    shr = (int *)shmat(shmid, NULL, 0);

    int matriks_satu[ukuran_baris_satu][ukuran_kolom_satu];
    int matriks_dua[ukuran_baris_dua][ukuran_kolom_dua];

    // Menghasilkan matriks acak
    for (int i = 0; i < ukuran_baris_satu; i++) {
        for (int j = 0; j < ukuran_kolom_satu; j++) {
            matriks_satu[i][j] = rand() % 5 + 1;
        }
    }

    for (int i = 0; i < ukuran_baris_dua; i++) {
        for (int j = 0; j < ukuran_kolom_dua; j++) {
            matriks_dua[i][j] = rand() % 4 + 1;
        }
    }

    printf("Matriks Pertama\n");
    for (int i = 0; i < ukuran_baris_satu; i++) {
        for (int j = 0; j < ukuran_kolom_satu; j++) {
            printf("%d ", matriks_satu[i][j]);
        }
        printf("\n");
    }
    printf("\nMatriks Kedua\n");
    for (int i = 0; i < ukuran_baris_dua; i++) {
        for (int j = 0; j < ukuran_kolom_dua; j++) {
            printf("%d ", matriks_dua[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    int hasil_perkalian[ukuran_baris_satu][ukuran_kolom_dua];
    int c = 0;
    for (int i = 0; i < ukuran_baris_satu; i++) {
        for (int j = 0; j < ukuran_kolom_dua; j++) {
            hasil_perkalian[i][j] = 0;
            for (int k = 0; k < ukuran_kolom_satu; k++) {
                hasil_perkalian[i][j] += matriks_satu[i][k] * matriks_dua[k][j];
            }
            shr[c] = hasil_perkalian[i][j];
            c++;
        }
    }

        printf("\nHasil Perkalian\n");
    for (int i = 0; i < ukuran_baris_satu; i++) {
        for (int j = 0; j < ukuran_kolom_dua; j++) {
            printf("%d ", shr[i * ukuran_kolom_dua + j]);
        }
        printf("\n");
    }
    
    shmdt((void *)shr);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
