
#include <stdio.h>
#include <sys/ipc.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/shm.h>
#include <unistd.h>

typedef unsigned long long ull;

void hitung_faktorial(int number) {
    int temp = 0, size = 0;
    int factorial_result[50];

    factorial_result[0] = 1;

    for (int i = 2; i <= number; i++) {
        temp = 0;
        // Menghitung faktorial dengan menyimpan sisa pembagian di temp
        for (int j = 0; j <= size; j++) {
            temp += factorial_result[j] * i;
            factorial_result[j] = temp % 10;
            temp /= 10;
        }

        // Menyimpan sisa pembagian pada index berikutnya jika masih ada
        while (temp > 0) {
            size++;
            factorial_result[size] = temp % 10;
            temp /= 10;
        }
    }

    // Mencetak hasil faktorial dari belakang ke depan
    for (int i = size; i >= 0; i--) {
        printf("%d", factorial_result[i]);
    }
    printf(" ");
}

int main() {
    key_t key = 1234;
    int *shared_memory;

    // Mengalokasikan shared memory
    int shared_memory_id = shmget(key, 20 * sizeof(int), IPC_CREAT | 0666);
    shared_memory = (int *)shmat(shared_memory_id, NULL, 0);

    int index = 0;
    printf("Hasil Perkalian\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", shared_memory[index]);
            index++;
        }
        printf("\n");
    }

    printf("Hasil Faktorial\n");
    for (int i = 0; i < 20; i++) {
        // Memanggil fungsi untuk menghitung faktorial
        hitung_faktorial(shared_memory[i]);
        if ((i+1) % 5 == 0) printf("\n");
    }

    return 0;
}

