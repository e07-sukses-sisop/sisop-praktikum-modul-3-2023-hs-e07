#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>
#include <ctype.h>  

#define PLAY_CMD 1
#define LIST_CMD 2
#define ADD_CMD 3
#define DECRYPT_CMD 4
#define CON_CMD 5

sem_t* sem;

struct msg_buffer {
    long msg_type;
    int msg_cmd_type;
    char msg_text[100];
    int msg_uid;
} message;
// 1 = Play, 2 = List, 3 = Add, 4 = Decrypt

void decodePlaylist(char* path) {
    FILE* fp = fopen(path, "r");
    char method[200];
    char song[200];
    char command[500];
    int count = 0;

    system("echo -n '' > playlist.txt");

    while(fscanf(fp, "%[^\n]", method) != EOF) {
        count++;
        getc(fp);
        if(strcmp(method, "rot13") == 0) {
            fscanf(fp, "%[^\n]", song);
            sprintf(command, "echo \"%s\" | tr 'A-Za-z' 'N-ZA-Mn-za-m' >> playlist.txt", song);
            system(command);
        } else if (strcmp(method, "base64") == 0) {
            fscanf(fp, "%[^\n]", song);
            sprintf(command, "echo \"%s\" | base64 -d >> playlist.txt", song);
            system(command);
            system("echo >> playlist.txt");
        } else {
            fscanf(fp, "%[^\n]", song);
            sprintf(command, "echo \"%s\" | xxd -r -p >> playlist.txt", song);
            system(command);
            system("echo >> playlist.txt");
        }
        getc(fp);
        
        printf("%s %s\n", method, song);
    }
    printf("total: %d\n", count);
}

void processPlaylist() {
    pid_t pid = fork();
    if(pid == 0) {
        if(sem_trywait(sem) != 0) {
            printf("STREAM SYSTEM OVERLOAD\n");
            exit(0);
        }
        // convert json into (method encoded) format
        system("awk 'BEGIN{FS = \"\\\"\"};/method/{print $4}; /song/{print $4}' song-playlist.json > temp.txt");
        decodePlaylist("temp.txt");
        system("rm temp.txt");
        system("cat playlist.txt | sort -f > temp.txt");
        system("cat temp.txt > playlist.txt");
        system("rm temp.txt");
        sem_post(sem);
        exit(0);
    }
}


void play(int uid, char* name) {
    pid_t pid = fork();
    if(pid > 0) return;
    if(pid < 0) return;
    if(sem_trywait(sem) != 0) {
        printf("STREAM SYSTEM OVERLOAD\n");
        exit(0);
    }
    char command[200];
    char* tempName = strtok(name, "\"");
    sprintf(command, "awk \"BEGIN{IGNORECASE = 1; count = 0} /%s/{arr[count++] = \\$0} END{print count; for (i = 0; i < count; i++) print arr[i]}\" playlist.txt >> temp.txt", tempName);
    system(command);
    FILE* fp = fopen("temp.txt", "r");
    int count;
    for(int i = 0; tempName[i] != '\0'; i++) {
        tempName[i] = toupper(tempName[i]);
    }
    fscanf(fp, "%d\n", &count);
    if(count == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", tempName);
        fclose(fp);
        system("rm temp.txt");
    } else if(count > 1) {
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\":\n", count, tempName);
        int i = 1;
        while(fscanf(fp, "%[^\n]", tempName) != EOF) {
            getc(fp);
            printf("%d. %s\n", i, tempName);
        }
        fclose(fp);
        system("rm temp.txt");
    } else {
        fscanf(fp ,"%[^\n]", tempName);
        getc(fp);
        for(int i = 0; tempName[i] != '\0'; i++) {
            tempName[i] = toupper(tempName[i]);
        }
        printf("USER %d PLAYING \"%s\"\n", uid, tempName);
        system("rm temp.txt");
        fclose(fp);
        sleep(5);
    }
    sem_post(sem);
    exit(0);
}

void list(int uid) {
    pid_t pid = fork();
    if(pid > 0) return;
    if(pid < 0) return;

    if(sem_trywait(sem) != 0) {
        printf("STREAM SYSTEM OVERLOAD\n");
        exit(0);
    }
    system("cat playlist.txt");
    sem_post(sem);
    exit(0);
}

void add(int uid, char* name) {
    pid_t pid = fork();
    if(pid > 0) return;
    if(pid < 0) return;

    if(sem_trywait(sem) != 0) {
        printf("STREAM SYSTEM OVERLOAD\n");
        exit(0);
    }
    
    char command[200];
    char* tempName = strtok(name, "\"");
    sprintf(command, "awk \"BEGIN{IGNORECASE = 1; found = 0} \\$0 == \\\"%s\\\"{found = 1} END{print found}\" playlist.txt >> temp.txt", tempName);
    system(command);
    
    FILE* fp = fopen("temp.txt", "r");
    int found = 0;
    fscanf(fp, "%d", &found);
    if(found) {
        printf("SONG ALREADY ON PLAYLIST\n");
        system("rm temp.txt");
        fclose(fp);
        sem_post(sem);
        exit(0);
    }
    sprintf(command, "echo '%s' >> playlist.txt", tempName);
    system(command);
    system("cat playlist.txt | sort -f > temp.txt");
    system("cat temp.txt > playlist.txt");
    system("rm temp.txt");
    printf("USER %d ADD %s\n", uid, tempName);
    sem_post(sem);
    fclose(fp);
    exit(0);
}

int main()
{
    // key for message queue
    key_t key;
    int msgid;

    // open semaphore
    sem = sem_open("playlist", O_CREAT, 0644, 2);
    sem_init(sem, 1, 2);
  
    // generate unique key
    key = ftok("progfile", 65);
    
  
    // create message queue
    msgid = msgget(key, 0666 | IPC_CREAT);



    while(1) {
        // message loop
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        switch(message.msg_cmd_type) {
        case PLAY_CMD:
            play(message.msg_uid, message.msg_text);
            break;
        case LIST_CMD:
            list(message.msg_uid);
            break;
        case ADD_CMD:
            add(message.msg_uid, message.msg_text);
            break;
        case DECRYPT_CMD:
            processPlaylist();
            break;
        case CON_CMD:
            if(sem_trywait(sem) != 0) {
                message.msg_type = 2;
                strcpy(message.msg_text, "STREAM SYSTEM OVERLOAD");
                msgsnd(msgid, &message, sizeof(message), 0);
                break;
            }
            sem_post(sem);
            break;
        default:
            printf("UNKNOWN COMMAND\n");
        }
    }
    // destryoy message queue
    msgctl(msgid, IPC_RMID, NULL);

    return 0;
}