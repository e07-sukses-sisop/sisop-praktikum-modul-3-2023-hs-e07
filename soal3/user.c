#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>
  
// msg struct
struct msg_buffer {
    long msg_type;
    int msg_cmd_type;
    char msg_text[100];
    int msg_uid;
} message;
  
int main()
{
    // key for message queue
    key_t key;
    int msgid;
    int user_id;
    char command[100];
    char song[100];
    
    
  
    // generate unique key
    key = ftok("progfile", 65);
  
    // create message queue
    msgid = msgget(key, 0666 | IPC_CREAT);

    while(1) {
        printf("Enter user id: ");
        scanf("%d", &user_id);
        
        message.msg_uid = user_id;
        
        message.msg_type = 1;
        message.msg_cmd_type = 5;
        strcpy(message.msg_text, " ");
        
        
        msgsnd(msgid, &message, sizeof(message), 0);
        msgrcv(msgid, &message, sizeof(message), 2, 0);
        
        if(message.msg_uid < 0) {
            printf("STREAM SYSTEM OVERLOAD!\n");
            continue;
        } else break;
    }

    // set message type for msgrcv
    message.msg_type = 1;
    
    while(1) {
        scanf("%s", command);
        if(strcmp(command, "PLAY") == 0) {
            message.msg_cmd_type = 1;
            scanf(" %[^\n]", song);
            getchar();
            strcpy(message.msg_text, song);
            
        } else if(strcmp(command, "LIST") == 0) {
            message.msg_cmd_type = 2;
            // msgsnd(msgid, &message, sizeof(message), 0);
        } else if(strcmp(command, "ADD") == 0) {
            message.msg_cmd_type = 3;
            scanf(" %[^\n]", song);
            getchar();
            strcpy(message.msg_text, song);
            // msgsnd(msgid, &message, sizeof(message), 0);
        } else if(strcmp(command, "DECRYPT") == 0) {
            message.msg_cmd_type = 4;
        } else {
            message.msg_cmd_type = -1;
        }
        msgsnd(msgid, &message, sizeof(message), 0);
    }
    return 0;
}