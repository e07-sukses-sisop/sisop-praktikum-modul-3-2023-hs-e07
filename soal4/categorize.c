#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

typedef struct Extension
{
    char name[10];
    int ct_file;
}
Ext;

Ext extList[100];
int ct_ext;
int ct_other;
int max_folder_size;

char *get_timestamp()
{
    time_t now = time(NULL);
    struct tm *tm_info = localtime(&now);
    char *timestamp = (char *) malloc(20 * sizeof(char));

    strftime(timestamp, 20, "%d-%m-%Y %H:%M:%S", tm_info);
    return timestamp;
}

void write_log(char *log)
{
    FILE *fp;
    fp = fopen("log.txt", "a");

    if(fp == NULL){
        printf("Gagal membuka file log.txt\n");
        exit(1);
    }
    fprintf(fp, "%s %s\n", get_timestamp(), log);
    fclose(fp);
}

void make_dir(char *dirname)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat folder %s\n", dirname);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan folder %s tidak berhenti dengan normal\n", dirname);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MADE %s", dirname + 2);
    write_log(log);
}

void make_file(char *filename)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"touch", filename, NULL};
        execv("/usr/bin/touch", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
}

void move_file(char *filename, char *dest, char *ext)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal memindahkan file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mv", filename, dest, NULL};
        execv("/usr/bin/mv", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pemindahan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MOVED %s file : %s > %s", ext, filename, dest + 2);
    write_log(log);
}

bool check_extension(char *filename, char *ext)
{
    int len_file = strlen(filename);
    int len_ext = strlen(ext);

    if(len_file <= len_ext){
        return false;
    }
    char temp[len_ext + 1];
    strcpy(temp, filename + len_file - len_ext);

    for(int i = 0; i < len_ext; ++i){
        if(isalpha(temp[i]))
            temp[i] = tolower(temp[i]);
    }
    return (strcmp(temp, ext) == 0);
}

void *categorizing(void *args)
{
    char *path = (char *) args;
    DIR *dir = opendir(path);
    struct dirent *ent;

    if(dir == NULL){
        printf("Gagal membuka direktori %s\n", path);
        pthread_exit(NULL);
    }
    while((ent = readdir(dir)) != NULL)
    {
        if(strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0){
            continue;
        }
        if(ent->d_type == DT_DIR)
        {
            char sub_dir[1000], log[2000];
            sprintf(sub_dir, "%s/%s", path, ent->d_name);

            sprintf(log, "ACCESSED %s", sub_dir);
            write_log(log);

            pthread_t thread;
            pthread_create(&thread, NULL, categorizing, (void *) sub_dir);
            pthread_join(thread, NULL);
        }
        else if(ent->d_type == DT_REG)
        {
            bool other = true;
            char file_path[1000];
            sprintf(file_path, "%s/%s", path, ent->d_name);

            for(int i = 0; i < ct_ext; ++i)
            {
                if(check_extension(ent->d_name, extList[i].name))
                {
                    char dest[1000];
                    extList[i].ct_file++;
                    int idx_folder = floor(extList[i].ct_file / max_folder_size);

                    if(idx_folder == 0)
                        sprintf(dest, "./categorized/%s", extList[i].name);
                    else {
                        sprintf(dest, "./categorized/%s (%d)", extList[i].name, idx_folder + 1);
                        if(access(dest, F_OK) != 0)
                            make_dir(dest);
                    }
                    move_file(file_path, dest, extList[i].name);
                    other = false;
                    break;
                }
            }
            if(other){
                move_file(file_path, "./categorized/other", "other");
                ct_other++;
            }
        }
    }
    closedir(dir);
    pthread_exit(NULL);
}

void read_files()
{
    char buffer[10];
    FILE *fp;
    fp = fopen("extensions.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file extensions.txt\n");
        exit(1);
    }
    while(fgets(buffer, 10, fp) != NULL)
    {
        if(buffer[strlen(buffer) - 1] == '\n')
            buffer[strlen(buffer) - 1] = '\0';
        strcpy(extList[ct_ext].name, buffer);
        ct_ext++;
    }
    for(int i = 0; i < ct_ext; ++i){
        extList[i].name[strlen(extList[i].name) - 1] = '\0';
    }
    fp = fopen("max.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file max.txt\n");
        exit(1);
    }
    while(fgets(buffer, 10, fp) != NULL)
        max_folder_size = atoi(buffer);
    fclose(fp);
}

void make_folders()
{
    make_dir("./categorized");
    for(int i = 0; i < ct_ext; ++i){
        char path[1000];
        sprintf(path, "./categorized/%s", extList[i].name);
        make_dir(path);
    }
    make_dir("./categorized/other");
}

void swap_Ext(Ext *a, Ext *b)
{
    Ext temp = *a;
    *a = *b;
    *b = temp;
}

void sort_Ext()
{
    for(int j = ct_ext - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(extList[i].ct_file > extList[i + 1].ct_file)
                swap_Ext(extList + i, extList + i + 1);
        }
    }
}

void print_Ext()
{
    for(int i = 0; i < ct_ext; ++i)
        printf("extension_%-5s : %d\n", extList[i].name, extList[i].ct_file);
    printf("%-15s : %d\n", "other", ct_other);
}

int main()
{
    read_files();
    make_folders();
    make_file("log.txt");

    char path[1000] = "files";
    pthread_t thread;
    pthread_create(&thread, NULL, categorizing, (void *) path);
    pthread_join(thread, NULL);

    sort_Ext();
    print_Ext();
    return 0;
}
