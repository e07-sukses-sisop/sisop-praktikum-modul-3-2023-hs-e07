#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

int ct_acc;
int ct_fd;
int ct_ext;

typedef struct Folder
{
    char name[1000];
    int ct_file;
}
FD;

typedef struct Extension
{
    char name[10];
    int ct_file;
}
Ext;

void swap_FD(FD *a, FD *b)
{
    FD temp = *a;
    *a = *b;
    *b = temp;
}

void swap_Ext(Ext *a, Ext *b)
{
    Ext temp = *a;
    *a = *b;
    *b = temp;
}

void sortAll(FD *fdList, Ext *extList)
{
    for(int j = ct_fd - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(fdList[i].ct_file > fdList[i + 1].ct_file)
                swap_FD(fdList + i, fdList + i + 1);
        }
    }
    for(int j = ct_ext - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(extList[i].ct_file > extList[i + 1].ct_file)
                swap_Ext(extList + i, extList + i + 1);
        }
    }
}

void printAll(FD *fdList, Ext *extList)
{
    printf("TOTAL ACCESSED : %d\n\n", ct_acc);

    printf("--- LIST FOLDER ---\n");
    for(int i = 0; i < ct_fd; ++i)
        printf("%-19s : %d\n", fdList[i].name, fdList[i].ct_file);
    printf("\n");

    printf("--- LIST EXTENSION ---\n");
    for(int i = 0; i < ct_ext; ++i)
        printf("%-5s : %d\n", extList[i].name, extList[i].ct_file);
    printf("\n");
}

void read_log(FD *fdList, Ext *extList)
{
    char buffer[2000];
    FILE *fp;
    fp = fopen("log.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file log.txt\n");
        exit(1);
    }
    while(fgets(buffer, 1000, fp) != NULL)
    {
        if(strstr(buffer, " ACCESSED ") != NULL)
            ct_acc++;
        else if(strstr(buffer, " MADE ") != NULL)
        {
            char fd[1000];
            sscanf(buffer, "%*s %*s %*s %[^\n]s", fd);
            fd[strlen(fd)] = '\0';

            int i;
            for(i = 0; i < ct_fd; ++i){
                if(strcmp(fd, fdList[i].name) == 0)
                    break;
            }
            if(i == ct_fd){
                strcpy(fdList[i].name, fd);
                fdList[i].ct_file = 0;
                ct_fd++;
            }
        }
        else if(strstr(buffer, " MOVED ") != NULL)
        {
            char ext[10], fd[1000];
            sscanf(buffer, "%*s %*s %*s %s", ext);

            char *temp = strstr(buffer, " > ");
            strcpy(fd, temp + 3);
            fd[strlen(fd) - 1] = '\0';

            int i;
            for(i = 0; i < ct_ext; ++i){
                if(strcmp(ext, extList[i].name) == 0){
                    extList[i].ct_file++;
                    break;
                }
            }
            if(i == ct_ext){
                strcpy(extList[i].name, ext);
                extList[i].ct_file = 1;
                ct_ext++;
            }
            for(i = 0; i < ct_fd; ++i){
                if(strcmp(fd, fdList[i].name) == 0){
                    fdList[i].ct_file++;
                    break;
                }
            }
        }
    }
    fclose(fp);
}

int main()
{
    FD fdList[400];
    Ext extList[100];
    read_log(fdList, extList);
    sortAll(fdList, extList);
    printAll(fdList, extList);
    return 0;
}
