#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

void download()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan download\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char link[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
        char *argv[] = {"wget", "-q", link, "-O", "hehe.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses download tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}

void unzip()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan unzip pada hehe.zip\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"unzip", "-q", "hehe.zip", NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses unzip pada hehe.zip tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}

int main()
{
    download();
    unzip();
    return 0;
}
